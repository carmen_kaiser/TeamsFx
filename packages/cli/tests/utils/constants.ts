// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.

export enum Capability {
  Notification = "notification",
  CommandBot = "command-bot",
  WorkflowBot = "workflow-bot",
  DashBoardTab = "dashboard-tab",
  Tab = "tab",
  SPFxTab = "tab-spfx",
  TabNonSso = "tab-non-sso",
  Bot = "bot",
  MessageExtension = "message-extension",
  M365SsoLaunchPage = "sso-launch-page",
  M365SearchApp = "search-app",
}
